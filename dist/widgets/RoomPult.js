function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { ButtonGroup, Button, Position, Popover, Intent, Classes, Icon } from "@blueprintjs/core";
import { withApollo } from "react-apollo";
import { compose } from "recompose";
import { withRouter } from "react-router";
import { isRole } from "react-pe-utilities";
import { __ } from "react-pe-utilities";

class RoomPult extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onDelete", evt => {
      this.props.onDelete(this.props.room.id);
    });

    this.state = {
      isPopover: false
    };
  }

  render() {
    console.log(this.props);
    const admin = isRole("administrator", this.props.user) ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ButtonGroup, null, /*#__PURE__*/React.createElement(Popover, {
      popoverClassName: Classes.POPOVER_CONTENT_SIZING,
      isOpen: this.state.isPopover === true ?
      /* Controlled */
      true :
      /* Uncontrolled */
      undefined,
      content: /*#__PURE__*/React.createElement("div", {
        className: "square"
      }, /*#__PURE__*/React.createElement("div", {
        className: ""
      }, /*#__PURE__*/React.createElement("div", {
        className: "mb-3"
      }, __("Remove room?")), /*#__PURE__*/React.createElement(Button, {
        intent: Intent.DANGER,
        onClick: this.onDelete
      }, __("Yes")))),
      position: Position.LEFT
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: "cross"
    }))))) : null;
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(ButtonGroup, null, admin));
  }

}

export default compose(withApollo, withRouter)(RoomPult);