function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Classes, Intent, Popover, Position } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { isRole } from "react-pe-utilities";

class TranslationMenu extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      isPopover: false
    });

    _defineProperty(this, "onDelete", () => {
      this.props.onDelete(this.props.translation.id);
    });
  }

  render() {
    return isRole("administrator", this.props.user) ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Popover, {
      popoverClassName: Classes.POPOVER_CONTENT_SIZING,
      isOpen: this.state.isPopover === true ?
      /* Controlled */
      true :
      /* Uncontrolled */
      undefined,
      content: /*#__PURE__*/React.createElement("div", {
        className: "square"
      }, /*#__PURE__*/React.createElement("div", {
        className: ""
      }, /*#__PURE__*/React.createElement("div", {
        className: "mb-3"
      }, __("Remove translation?")), /*#__PURE__*/React.createElement(Button, {
        intent: Intent.DANGER,
        onClick: this.onDelete
      }, __("Yes")))),
      position: Position.RIGHT_TOP
    }, /*#__PURE__*/React.createElement(Button, {
      rightIcon: "cross",
      minimal: true,
      title: __("Delete Translation")
    }))) : null;
  }

}

export default TranslationMenu;