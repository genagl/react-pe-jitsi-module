function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { ButtonGroup, Button, Dialog, Intent, Collapse } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";
import { isRole } from "react-pe-utilities";

class TranslationPult extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onClick", id => {
      this.props.onToggle(id);
    });

    _defineProperty(this, "onTitle", evt => {
      this.setState({
        newTitle: evt.currentTarget.value
      });
    });

    _defineProperty(this, "onHide", evt => {
      this.setState({
        newHide: !this.state.newHide
      });
    });

    _defineProperty(this, "onPsw", evt => {
      this.setState({
        psw: evt.currentTarget.value
      });
    });

    _defineProperty(this, "onJump", evt => {
      this.setState({
        newJump: !this.state.newJump
      });
    });

    _defineProperty(this, "onOpen", evt => {
      this.setState({
        newTitle: __("Room ") + (this.state.pe_room.length + 1),
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onNew", () => {
      this.props.onNew({
        title: this.state.newTitle,
        isHidden: this.state.newHide,
        psw: this.state.psw,
        isJump: this.state.newJump
      });
    });

    this.state = {
      current: this.props.translation.pe_room && this.props.translation.current ? this.props.translation.current : "dfrhth",
      pe_room: this.props.translation.pe_room || [],
      isOpen: false,
      newHide: false,
      newJump: false,
      psw: "",
      newTitle: ""
    };
  }

  componentWillUpdate(nextProps) {
    if (nextProps.translation.pe_room !== this.state.pe_room || nextProps.translation.current !== this.state.current) {
      const state = {};

      if (nextProps.translation.pe_room !== this.state.pe_room) {
        state.pe_room = nextProps.translation.pe_room;
      }

      if (nextProps.translation.current !== this.state.current) {
        // console.log( nextProps.translation.current, this.state.current );
        state.current = nextProps.translation.current;
      }

      this.setState(state);
    }
  }

  render() {
    // console.log( this.props.translation );
    const pe_room = this.state.pe_room ? /*#__PURE__*/React.createElement(ButtonGroup, {
      vertical: true,
      fill: true
    }, this.state.pe_room.map((e, i) => {
      const isCurrent = this.state.current === e.external_id;
      let upr;

      if (isCurrent) {
        const members = e.members.map((ee, ii) => {
          const moder = ee.moderator ? /*#__PURE__*/React.createElement("div", {
            className: "moder-label",
            title: __("Room Moderator")
          }, "M") : null;
          const admin = ee.admin ? /*#__PURE__*/React.createElement("div", {
            className: "admin-label",
            title: __("Translation Administrator")
          }, "A") : null;
          return /*#__PURE__*/React.createElement("div", {
            className: "room-member",
            key: ii,
            "member-id": ee._id
          }, /*#__PURE__*/React.createElement("div", {
            className: "d-flex align-items-center justify-content-start"
          }, /*#__PURE__*/React.createElement("div", {
            className: "avatar",
            style: {
              backgroundImage: `url(${ee.avatar})`
            }
          }, admin, moder), /*#__PURE__*/React.createElement("div", {
            className: `${this.props.translation.me === ee._id ? "text-danger " : ""}member-name`
          }, ee.name, /*#__PURE__*/React.createElement("div", {
            className: "room-member-pult"
          }, initArea("room-member", { ...this.props,
            room: e,
            member: ee
          })))));
        });
        upr = /*#__PURE__*/React.createElement("div", {
          className: "room-upr animated animation-opened p-3 "
        }, /*#__PURE__*/React.createElement("div", {
          className: ""
        }, __("Members"), ":"), members, initArea("room", { ...this.props,
          room: e
        }));
      }

      const memberLabel = isCurrent ? /*#__PURE__*/React.createElement("span", {
        className: "members-col"
      }, e.members ? e.members.length : null) : null;
      return /*#__PURE__*/React.createElement("div", {
        key: i,
        className: "border-bottom"
      }, /*#__PURE__*/React.createElement(Button, {
        intent: isCurrent ? Intent.PRIMARY : Intent.SECONDARY,
        className: "transition-300 justify-content-space-between pl-3 pr-5 pe-room-btn",
        fill: true,
        minimal: !isCurrent,
        large: true,
        onClick: () => this.onClick(e.external_id),
        icon: e.is_locked ? "lock" : null
      }, e.post_title, memberLabel), upr);
    })) : null;
    const new_btn = isRole("administrator", this.props.user) ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Button, {
      fill: true,
      text: __("Create room"),
      minimal: true,
      intent: Intent.SUCCESS,
      onClick: this.onOpen
    })) : null;
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "widget-title"
    }, __("Rooms"), ":"), /*#__PURE__*/React.createElement("div", null, pe_room), /*#__PURE__*/React.createElement("div", {
      className: "mt-3"
    }, new_btn), /*#__PURE__*/React.createElement(Dialog, {
      title: __("Insert new Room Parameters"),
      isOpen: this.state.isOpen,
      onClose: this.onOpen,
      className: "maxw-550"
    }, /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-5 d-flex align-items-center  justify-content-end"
    }, __("Title")), /*#__PURE__*/React.createElement("div", {
      className: "col-md-7"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      onChange: this.onTitle,
      value: this.state.newTitle,
      className: "form-control input dark"
    })), /*#__PURE__*/React.createElement("div", {
      className: "col-md-5 d-flex align-items-center  justify-content-end"
    }, __("Hide by password?")), /*#__PURE__*/React.createElement("div", {
      className: "col-md-7  mb-4"
    }, /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      onChange: this.onHide,
      value: this.state.newHide,
      className: "checkbox",
      id: "newHide"
    }), /*#__PURE__*/React.createElement("label", {
      htmlFor: "newHide"
    }))), /*#__PURE__*/React.createElement(Collapse, {
      className: "overflow-hidden",
      isOpen: this.state.newHide
    }, /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-5 d-flex align-items-center  justify-content-end"
    }, __("Password")), /*#__PURE__*/React.createElement("div", {
      className: "col-md-7  mb-4"
    }, /*#__PURE__*/React.createElement("input", {
      type: "password",
      onChange: this.onPsw,
      value: this.state.psw,
      className: "form-control input dark",
      id: "psw"
    })))), /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-5 d-flex align-items-center  justify-content-end"
    }, __("Jump to Room after creation?")), /*#__PURE__*/React.createElement("div", {
      className: "col-md-7  mb-4"
    }, /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      onChange: this.onJump,
      value: this.state.newJump,
      className: "checkbox",
      id: "newJump"
    }), /*#__PURE__*/React.createElement("label", {
      htmlFor: "newJump"
    })), /*#__PURE__*/React.createElement("div", {
      className: "col-md-7 offset-md-5 mt-5"
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onNew
    }, __("Insert new Room")))))));
  }

}

export default TranslationPult;