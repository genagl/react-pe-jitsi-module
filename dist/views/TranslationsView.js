function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withApollo, Query } from "react-apollo";
import { compose } from "recompose";
import moment from "moment";
import { withRouter } from "react-router";
import { Button, Intent, Tooltip, Position, Callout } from "@blueprintjs/core";
import { Loading } from 'react-pe-useful';
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";
import { getQueryArgs, getQueryName, queryCollection } from "react-pe-layouts";
import { initArea } from "react-pe-utilities";

class TranslationsView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "transls", void 0);

    _defineProperty(this, "onStart", id => {
      this.props.history.push(`/translation/${id}`);
    });
  }

  render() {
    const data_type = "PE_Translation";
    /*
    const query_name = getQueryName(data_type)
    const query_args = getQueryArgs(data_type)
      const query = queryCollection( data_type, query_name, query_args );
      this.props.client.query({
      query: query,
      variables: {},
    }).then(result => {
      //console.log(result);
    })
    //console.log(this.props);
    */

    const translations = [];
    console.log(this.props);
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "layout-state p-0"
    }, /*#__PURE__*/React.createElement("div", {
      className: "container joppa position-relative"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row mt-4"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }, /*#__PURE__*/React.createElement("h1", null, __("Translations")), initArea("aside-left", { ...this.props,
      translations,
      onStart: this.onStart,
      route: this.props.location.pathname
    })), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9 m-main pt-5"
    }, /*#__PURE__*/React.createElement(Callout, null, __("Вы можете начать новую трансляцию, нажав кнопку «Начать трансляцию». Вы можете присоединиться к трансляциям, список которых указаны на виджете. Чтобы создавать или  участвовать в трансляциях не надо иметь учётной записи на нашем портале.")), /*#__PURE__*/React.createElement("div", {
      className: "mt-4"
    }, this.list(data_type)))))));
  }

  list(data_type) {
    const query_name = getQueryName(data_type);
    const query_args = getQueryArgs(data_type);
    const query = queryCollection(data_type, query_name, query_args);
    return /*#__PURE__*/React.createElement(Query, {
      query: query
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        this.transls = data[query_name] || []; // console.log(this.transls);

        const translations = this.transls.length > 0 ? this.transls.map((e, i) => {
          const start_date = e.start_date ? moment(e.start_date).format("D.MM.YYYY HH:mm") : null;
          const end_date = e.end_date ? moment(e.end_date).format("D.MM.YYYY HH:mm") : null;
          return /*#__PURE__*/React.createElement("div", {
            className: "row mb-1 border",
            key: i
          }, /*#__PURE__*/React.createElement("div", {
            className: "col-4 p-3"
          }, /*#__PURE__*/React.createElement("div", {
            className: "title"
          }, e.post_title), /*#__PURE__*/React.createElement("div", {
            className: "date"
          }, /*#__PURE__*/React.createElement("div", null, start_date), /*#__PURE__*/React.createElement("div", null, end_date))), /*#__PURE__*/React.createElement("div", {
            className: "col-7 p-3 d-flex align-items-center",
            dangerouslySetInnerHTML: {
              __html: e.post_content
            }
          }), /*#__PURE__*/React.createElement("div", {
            className: "col-1 d-flex p-1 flex-column justify-content-end hidden"
          }, /*#__PURE__*/React.createElement(Tooltip, {
            content: __("follow"),
            position: Position.LEFT,
            className: "d-flex justify-content-end"
          }, /*#__PURE__*/React.createElement(Button, {
            fill: true,
            className: "mb-1",
            icon: "follower",
            minimal: true
          })), /*#__PURE__*/React.createElement(Tooltip, {
            content: __("favorite"),
            position: Position.LEFT,
            className: "d-flex justify-content-end"
          }, /*#__PURE__*/React.createElement(Button, {
            fill: true,
            className: "mb-0",
            icon: "heart",
            minimal: true
          }))));
        }) : /*#__PURE__*/React.createElement(Callout, {
          intent: Intent.WAITING
        }, __("No items"));
        return /*#__PURE__*/React.createElement("div", {
          className: "w-100 px-3"
        }, translations);
      }

      if (error) {
        return error.toString();
      }
    });
  }

}

export default compose(withApollo, withRouter)(TranslationsView);