function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Intent } from "@blueprintjs/core";
import { withRouter } from "react-router";
import { compose } from "recompose";
import Jitsi from "./Jitsi";
import AvatarChooser from "./AvatarChooser";
import { __ } from "react-pe-utilities";
import { AppToaster } from 'react-pe-useful';

class Translation extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onPassword", evt => {
      this.setState({
        password: evt.currentTarget.value
      });
    });

    _defineProperty(this, "onEnter", () => {
      const currr = this.state.translation.pe_room.filter(e => e.external_id === this.state.translation.current); // console.log(this.state.password, currr[0].password, this.state.password === currr[0].password);

      if (currr.length < 1) {
        this.setState({
          descr: "No right!"
        });
      }

      if (this.state.password === currr[0].password) {
        this.setState({
          is_locked: false
        });
      } else {
        this.setState({
          descr: "No right!"
        });
      }
    });

    _defineProperty(this, "onName", e => this.setState({
      display_name: e.currentTarget.value
    }));

    _defineProperty(this, "onAvatar", avatarUrl => {
      this.setState({
        avatar: avatarUrl
      });
    });

    _defineProperty(this, "onEnterName", () => {
      if (this.state.display_name) {
        // console.log( this.state.avatar );
        this.setState({
          user: {
            display_name: this.state.display_name,
            avatar: this.state.avatar
          }
        });
      } else {
        AppToaster.show({
          intent: Intent.DANGER,
          icon: "tick",
          className: " ",
          message: __("Name field not be empty")
        });
      }
    });

    _defineProperty(this, "getTranslation", data => {
      const translaton = { ...data
      };
      return translaton;
    });

    _defineProperty(this, "onLeave", evt => {
      // console.log('onLeave');
      // this.props.history.push("/translations");
      if (this.props.onLeave) this.props.onLeave(evt);
    });

    _defineProperty(this, "onChange", (name, data) => {
      // console.log('onChange');
      // this.props.history.push("/translations");
      if (this.props.onChange) this.props.onChange(name, data);
    });

    _defineProperty(this, "onJoin", data => {
      if (this.props.onJoin) this.props.onJoin(data);
    });

    _defineProperty(this, "participantJoined", data => {
      if (this.props.participantJoined) this.props.participantJoined(data);
    });

    _defineProperty(this, "participantKickedOut", data => {
      if (this.props.participantKickedOut) this.props.participantKickedOut(data);
    });

    _defineProperty(this, "participantLeft", data => {
      if (this.props.participantLeft) this.props.participantLeft(data);
    });

    _defineProperty(this, "feedbackSubmitted", data => {
      if (this.props.feedbackSubmitted) this.props.feedbackSubmitted(data);
    });

    this.state = {
      avatar: this.props.user && this.props.user.avatar ? this.props.user.avatar : "/assets/img/user1.svg",
      display_name: this.props.user ? this.props.user.display_name : null,
      user: this.props.user || {},
      translation: this.props.translation,
      is_locked: true
    };
  }

  componentWillUpdate(nextProps) {
    if (nextProps.translation !== this.state.translation) {
      this.setState({
        translation: nextProps.translation,
        is_locked: nextProps.translation.current !== this.state.translation.current,
        descr: "",
        password: ""
      });
    }
  }

  render() {
    // console.log( this.state.is_locked );
    const currr = this.state.translation.pe_room.filter(e => e.external_id === this.state.translation.current);
    let is_locked = false;

    if (currr.length > 0) {
      is_locked = currr[0].is_locked && this.state.is_locked;
    }

    return this.state.user.display_name ? /*#__PURE__*/React.createElement("div", {
      className: "translation-screen",
      id: this.props.id
    }, is_locked ? /*#__PURE__*/React.createElement("div", {
      className: "card p-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text-center title"
    }, __("Insert password")), /*#__PURE__*/React.createElement("input", {
      type: "password",
      value: this.state.password,
      onChange: this.onPassword,
      className: "form-control my-3"
    }), /*#__PURE__*/React.createElement(Button, {
      text: __("Enter"),
      onClick: this.onEnter
    }), /*#__PURE__*/React.createElement("div", {
      className: "text-danger text-center small mt-2"
    }, this.state.descr)) : /*#__PURE__*/React.createElement(Jitsi, {
      user: this.state.user,
      translation: this.getTranslation(this.state.translation),
      onLeave: this.onLeave,
      onChange: this.onChange,
      onJoin: this.onJoin,
      participantJoined: this.participantJoined,
      participantKickedOut: this.participantKickedOut,
      participantLeft: this.participantLeft,
      feedbackSubmitted: this.feedbackSubmitted,
      height: this.props.height,
      containerStyle: this.props.containerStyle
    })) : /*#__PURE__*/React.createElement("div", {
      className: "translation-screen",
      id: this.props.id
    }, /*#__PURE__*/React.createElement("div", {
      className: "title "
    }, __("How you call?")), /*#__PURE__*/React.createElement("div", {
      className: "form-group justify-content-center d-flex align-items-center my-4"
    }, /*#__PURE__*/React.createElement("label", {
      className: "exampleInputEmail1 mr-3 mb-0"
    }, __("Name")), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "form-control",
      placeholder: __("Name"),
      value: this.state.display_name,
      onChange: this.onName
    })), /*#__PURE__*/React.createElement("div", {
      className: " my-4"
    }, /*#__PURE__*/React.createElement("label", {
      className: "exampleInputEmail1 mr-3 mb-0"
    }, __("Avatar")), /*#__PURE__*/React.createElement(AvatarChooser, {
      onChoose: this.onAvatar
    })), /*#__PURE__*/React.createElement("div", {
      className: "justify-content-center d-flex "
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onEnterName
    }, __("Enter"))));
  }

}

export default compose(withRouter)(Translation);