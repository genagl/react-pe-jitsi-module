import React, { Component } from "react";
import Translation from "./Translation";

class TranslationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentID: this.props.currentID
    };
  }

  render() {
    return this.props.currentID ? /*#__PURE__*/React.createElement(Translation, {
      id: this.props.currentID
    }) : /*#__PURE__*/React.createElement("div", {
      className: "empty-translation"
    });
  }

}

export default TranslationScreen;