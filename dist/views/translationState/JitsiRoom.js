function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { __ } from "react-pe-utilities";
import Translation from "./Translation";

class JitsiRoom extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onLeave", evt => {
      if (this.props.onLeave) this.props.onLeave(evt);
    });

    _defineProperty(this, "onChange", (name, data) => {
      if (this.props.onChange) this.props.onChange(name, data);
    });

    _defineProperty(this, "onJoin", data => {
      if (this.props.onJoin) this.props.onJoin(data);
    });

    _defineProperty(this, "participantJoined", data => {
      if (this.props.participantJoined) this.props.participantJoined(data);
    });

    _defineProperty(this, "participantKickedOut", data => {
      if (this.props.participantKickedOut) this.props.participantKickedOut(data);
    });

    _defineProperty(this, "participantLeft", data => {
      if (this.props.participantLeft) this.props.participantLeft(data);
    });

    _defineProperty(this, "feedbackSubmitted", data => {
      if (this.props.feedbackSubmitted) this.props.feedbackSubmitted(data);
    });
  }

  render() {
    console.log(this.props);
    const {
      jitsi,
      jitsi_password,
      post_title,
      id,
      user
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: "course-talk-room-cont"
    }, /*#__PURE__*/React.createElement(Translation, {
      user: user,
      translation: {
        post_title,
        current: jitsi,
        external_id: jitsi,
        password: jitsi_password,
        is_locked: true,
        pe_room: [{
          external_id: jitsi,
          members: []
        }]
      },
      onLeave: this.onLeave,
      onChange: this.onChange,
      onJoin: this.onJoin,
      participantJoined: this.participantJoined,
      participantKickedOut: this.participantKickedOut,
      participantLeft: this.participantLeft,
      feedbackSubmitted: this.feedbackSubmitted
    }));
  }

}

export default compose(withRouter)(JitsiRoom);