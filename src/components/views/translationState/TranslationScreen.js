import React, { Component } from "react"
import Translation from "./Translation"

class TranslationScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentID: this.props.currentID,
    }
  }

  render() {
    return this.props.currentID
      ?		<Translation id={this.props.currentID} />
      :		<div className="empty-translation" />
  }
}
export default TranslationScreen
