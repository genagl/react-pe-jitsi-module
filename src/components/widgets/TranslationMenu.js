import React, { Component } from "react"
import { Button, Classes, Intent, Popover, Position, } from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import { isRole } from "react-pe-utilities"

class TranslationMenu extends Component {
  state = {
    isPopover: false,
  }

  render() {
    return isRole("administrator", this.props.user)
      ? (
        <>
          <Popover
            popoverClassName={Classes.POPOVER_CONTENT_SIZING}
            isOpen={this.state.isPopover === true ? /* Controlled */ true : /* Uncontrolled */ undefined}
            content={(
              <div className="square">
                <div className="">
                  <div className="mb-3">
                    {__("Remove translation?")}
                  </div>
                  <Button intent={Intent.DANGER} onClick={this.onDelete}>
                    {__("Yes")}
                  </Button>
                </div>
              </div>
            )}
            position={Position.RIGHT_TOP}
          >
            <Button rightIcon="cross" minimal title={__("Delete Translation")} />
          </Popover>
        </>
      )
      : null
  }

  onDelete = () => {
    this.props.onDelete(this.props.translation.id)
  }
}

export default TranslationMenu
