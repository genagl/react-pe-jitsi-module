import React, { Component } from "react"
import {
  ButtonGroup, Button, Position, Popover, Intent, Classes, Icon,
} from "@blueprintjs/core"
import { withApollo } from "react-apollo"
import { compose } from "recompose"
import { withRouter } from "react-router"
import { isRole } from "react-pe-utilities"
import { __ } from "react-pe-utilities"

class RoomPult extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isPopover: false,
    }
  }

  render() {
    console.log(this.props)
    const admin = isRole("administrator", this.props.user)
      ?			(
        <>
          <ButtonGroup>
            <Popover
              popoverClassName={Classes.POPOVER_CONTENT_SIZING}
              isOpen={this.state.isPopover === true ? /* Controlled */ true : /* Uncontrolled */ undefined}
              content={(
                <div className="square">
                  <div className="">
                    <div className="mb-3">
              {__("Remove room?")}
            </div>
                    <Button intent={Intent.DANGER} onClick={this.onDelete}>
              {__("Yes")}
            </Button>
                  </div>
                </div>
    )}
              position={Position.LEFT}
            >
              <Button minimal>
                <Icon icon="cross" />
              </Button>
            </Popover>
          </ButtonGroup>
        </>
      )
      :			null
    return (
      <div>
        <ButtonGroup>
          {admin}
        </ButtonGroup>
      </div>
    )
  }

	onDelete = (evt) => {
	  this.props.onDelete(this.props.room.id)
	}
}
export default compose(
  withApollo,
  withRouter,
)(RoomPult)
