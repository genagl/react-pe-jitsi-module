# react-pe-jitsi-module


## Установка

1. В корневой папке приложения запускаем

``` 
npm run add-module pe-jitsi-module https://gitlab.com/genagl/react-pe-jitsi-module
```

 2. Запускаем локальную сессию:

``` 
npm start
```

 или компиллируем приложение для последующего размещения на сервере:

``` 
npm run build 
```

 ## Структура и сценарии использования:

 [см.](http://ux.protopia-home.ru/)